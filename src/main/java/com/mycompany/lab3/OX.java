/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author informatics
 */
class OX {
    static String[][] Table = {{"_","_","_"},{"_","_","_"},{"_","_","_"}};

    static boolean Checkwin(String[][] Table, String CP) {   
        
        if (checkRow(Table, CP)) {
            return true;
        }
        if (checkgy(Table, CP)){
            return true;
        }
        if (checkgy2(Table, CP)){
            return true;
        }
    return false;
    }
    private static boolean checkRow(String[][] Table, String CP) {
        for (int row = 0; row < 3; row ++) {
            if (checkRow(Table,CP,row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] Table, String CP, int row) {
        return Table[row][0].equals(CP)&& Table[row][1].equals(CP)&& Table[row][2].equals(CP);
    }
    private static boolean checkgy(String[][] Table, String CP){
        return Table[0][0].equals(CP)&& Table[1][1].equals(CP)&& Table[2][2].equals(CP);
    }
    private static boolean checkgy2(String[][] Table, String CP){
        return Table[0][2].equals(CP)&& Table[1][1].equals(CP)&& Table[2][0].equals(CP);
    }
    public static boolean checkdraw(String[][] Table) {
       return Table[0][0] != " " && Table[0][1] != " " && Table[0][2] != " " &&Table[1][0] != " " && Table[1][1] != " " && Table[1][2] != " " &&Table[2][0] != " " && Table[2][1] != " " && Table[2][2] != "_";
    }

    public static String[][] getTable() {
        return Table;
    }
}
